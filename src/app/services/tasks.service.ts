import { Injectable } from '@angular/core';
import { Task } from '../models/task.model';

@Injectable({
  providedIn: 'root'
})
export class TasksService {
  public tasks: Task[];

  constructor() {
    this.tasks = [];
  }

  public getTasks(): Task[] {
    return this.tasks;
  }

  public getFindTask(id: number): Task {
    return this.tasks.find(task => task.id === id);
  }

  public createTask(task: Task): void {
    task.id = this.tasks.length + 1;
    this.tasks.push(task);
  }

  public deleteTask(id: number): void {
    this.tasks = this.tasks.filter(task => task.id !== id);
    console.log(this.tasks);
  }
}
