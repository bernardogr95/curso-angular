import { Component, Input, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task.model';
import { TasksService } from 'src/app/services/tasks.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html'
})
export class TaskComponent implements OnInit {
  @Input() task: Task;
  @Input() done: boolean;

  constructor(public tasksService: TasksService) { }

  ngOnInit(): void {
  }

  deleteTask(task: Task): void {
    console.log('deleteTask');
    this.tasksService.deleteTask(task.id);
  }
}
