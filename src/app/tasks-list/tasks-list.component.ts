import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Task } from '../models/task.model';
import { TasksService } from '../services/tasks.service';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html'
})
export class TasksListComponent implements OnInit {
  public title: string;
  public newTask: Task;

  constructor(public tasksService: TasksService) { }

  ngOnInit(): void {
    this.title = "Lista de tareas";
    this.newTask = new Task('', false); 
  }

  public onSubmitTask() {
    this.tasksService.createTask({...this.newTask})
    this.newTask.description = '';
  }

}
